<?php

namespace App\Tests;



use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testVoiture()
    {
        $voiture = new Voiture();

        $voiture->setSerie('ABC123');
	$voiture->setDateMiseEnMarche(new \DateTime('2023-01-01'));
        $voiture->setModele('Corolla');
        $voiture->setPrixJour(50);

        $this->assertTrue($voiture->getSerie() === 'ABC123');
	$this->assertEquals(new \DateTime('2023-01-01'), $voiture->getDateMiseEnMarche());
        $this->assertTrue($voiture->getModele() === 'Corolla');
	$this->assertEquals(50, $voiture->getPrixJour());
    }
}
