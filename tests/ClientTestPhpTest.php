<?php

namespace App\Tests;




use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClient()
    {
        $client = new Client();

        $client->setCin('123456789');
        $client->setNom('Doe');
        $client->setPrenom('John');
        $client->setAdresse('123 Main St');

        $this->assertTrue($client->getCin() === '123456789');
        $this->assertTrue($client->getNom() === 'Doe');
        $this->assertTrue($client->getPrenom() === 'John');
        $this->assertTrue($client->getAdresse() === '123 Main St');
    }
}
