<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Client;
use App\Entity\Voiture;
use App\Entity\Location;

class IntegrationTest extends KernelTestCase
{
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testEntities()
    {
        // Create Client entity
        $client = new Client();
        $client->setCin(123456789);
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdresse('123 Main St');

        // Create Voiture entity
        $voiture = new Voiture();
        $voiture->setSerie('ABC123');
        $voiture->setDateMiseEnMarche(new \DateTime('2023-01-01'));
        $voiture->setModele('Sedan');
        $voiture->setPrixJour(50.00);

        // Create Location entity
        $location = new Location();
        $location->setDateDebut(new \DateTime('2023-01-01'));
        $location->setDateRetour(new \DateTime('2023-01-10'));
        $location->setPrix(500.00);
        $location->setVoiture($voiture);
        $location->setClient($client);

        // Persist entities
        $this->entityManager->persist($client);
        $this->entityManager->persist($voiture);
        $this->entityManager->persist($location);
        $this->entityManager->flush();

        // Retrieve entities from the database
        $persistedClient = $this->entityManager->getRepository(Client::class)->findOneBy(['id' => $client->getId()]);
        $persistedVoiture = $this->entityManager->getRepository(Voiture::class)->findOneBy(['id' => $voiture->getId()]);
        $persistedLocation = $this->entityManager->getRepository(Location::class)->findOneBy(['id' => $location->getId()]);

        // Assert that entities are persisted correctly
        $this->assertEquals($client->getCin(), $persistedClient->getCin());
        $this->assertEquals($voiture->getSerie(), $persistedVoiture->getSerie());
        $this->assertEquals($location->getPrix(), $persistedLocation->getPrix());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // Clean up the database
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
